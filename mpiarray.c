#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

/*  mpiarray
	Copyright (C) 2022  Sigenae-INRAE
	https://forgemia.inra.fr/patrice.dehais/mpiarray/-/blob/master/LICENSE
	*/


void error(char *msg) {
	fprintf(stderr,"%s\n",msg);
	exit(1);
}

int main(int argc, char** argv) {
	#define BUFFERSIZE 1024
	FILE *fp;
	char buffer[BUFFERSIZE];
	int arg_max;
	char *line_buffer=NULL;
	int line_no=-1;
	int res;
	
	// check parameter
	if (argc != 2) error("Error: need a text file as parameter with shell command lines");
	// retrieve max shell command line
	if ((fp=popen("getconf ARG_MAX", "r"))==NULL ||
		fgets(buffer,BUFFERSIZE,fp)==NULL ||
		(arg_max=atoi(buffer)) <= 0
		) error("Can't retrieve max length (ARGMAX) shell command line");
	pclose(fp);
	
	// allocate line buffer
	if ((line_buffer=(char*)malloc(arg_max))==NULL) error("Can't alloc line buffer");
	
	// open command line file
	if ((fp=fopen(argv[1],"r"))==NULL) error("Can't open shell command lines file");	
	
	// Initialize the MPI environment. The two arguments to MPI Init are not
	// currently used by MPI implementations, but are there in case future
	// implementations might need the arguments.
	MPI_Init(NULL, NULL);

	// Get the number of processes
	int world_size;
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);

	// Get the rank of the process
	int world_rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

	// Get the name of the processor
	char processor_name[MPI_MAX_PROCESSOR_NAME];
	int name_len;
	MPI_Get_processor_name(processor_name, &name_len);

	// loop on command lines and execute only those having good rank
	while (fgets(line_buffer,arg_max,fp) != NULL) {
		line_no++;
		if ((line_no % world_size) != world_rank) continue;
		res=system(line_buffer);
		fprintf(stderr,"line %d on node %s %d/%d with status %d\n",line_no+1,processor_name,world_rank,world_size,res);
	}
		
	// Finalize the MPI environment. No more MPI calls can be made after this
	MPI_Finalize();
	
	fclose(fp);
}
