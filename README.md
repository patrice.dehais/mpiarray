# mpiarray

Launch as many as possibles shell command lines in parallel via MPI in a quite simple way:
- put shell command lines in a text file
- each line will be run in parallel (as much as MPI processors)


Two implementations are available:
- one in C
- one in python

### C implementation

mpicc -o mpiarray mpiarray.c

### Python3 implementation

require mpi4py library

### Usage

    mpiarray my_cmds.txt
    mpiarray.py my_cmds.txt

