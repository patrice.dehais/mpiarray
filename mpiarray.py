#!/bin/env python3
from mpi4py import MPI
import sys
import os
import re

"""
  mpiarray
  Copyright (C) 2022  Sigenae-INRAE
  https://forgemia.inra.fr/patrice.dehais/mpiarray/-/blob/master/LICENSE
"""

def error(*msg):
  """ print an error message
  """
  print(*msg,file=sys.stderr)
  sys.exit(1)



if len(sys.argv) != 2:
  error("Usage: %s command lines file" % (sys.argv[0]))

try:
  cmd_file = open(sys.argv[1],"r")
except:
  error("Error: can't read file %s" % (sys.argv[0]))
  
  
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
nb_proc = comm.Get_size()
proc_name = MPI.Get_processor_name()

line_no=-1
for line in cmd_file.readlines():
  line_no+=1
  if line_no % nb_proc != rank:
	  continue
  if re.match('^\s*$', line):
    continue
  res=os.system(line)
  print("line %d on node %s rank %d/%d with status %d" % (line_no+1,proc_name,rank,nb_proc,res), file=sys.stderr)

cmd_file.close()


